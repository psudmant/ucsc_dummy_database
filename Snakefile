from difflib import SequenceMatcher
import pandas as pd


def get_max_match(ucsc_contig, ens_contigs):
    
    scores = [ SequenceMatcher(lambda x: x==" ", ucsc_contig, ens_contig).ratio() for ens_contig in ens_contigs]
                                
    return ens_contigs[scores.index(max(scores))], max(scores)

rule all:
    input:
        "UCSC/rattus_norvegicus/rn5/database/ucscToEnsembl.txt.gz",
        "UCSC/macaca_mulatta/rheMac2/database/ucscToEnsembl.txt.gz",
        "UCSC/homo_sapiens/hg19/liftover/hg19ToGalGal4.over.chain.gz",
        "UCSC/homo_sapiens/hg38/database/ucscToEnsembl.txt.gz"

rule gzip_hg38_UCSC_to_Ensembl:
    input:
        "UCSC/homo_sapiens/hg38/database/ucscToEnsembl.txt"
    output:
        "UCSC/homo_sapiens/hg38/database/ucscToEnsembl.txt.gz"
    shell:
        "gzip {input[0]}"

rule make_hg38_UCSC_to_Ensembl:
    input:
        "homo_sapiens/ucscToINSDC.txt.gz"
    output:
        "UCSC/homo_sapiens/hg38/database/ucscToEnsembl.txt"
    run:
        t = pd.read_csv(input[0], sep="\t", names=["UCSC", 'start', 'end', 'INSDC'], compression="gzip")
        contigs = {"chr%d"%i:str(i) for i in range(1,23)}
        contigs["chrX"] =  "X"
        contigs["chrY"] = "Y"
        contigs["chrMT"] = "MT"

        for i,row in t.iterrows():
            if not row['UCSC'] in contigs:
                contigs[row['UCSC']] = row['INSDC']
        
        new_rows = []
        for UCSC, ENS in contigs.items():
            new_rows.append({"UCSC":UCSC, "ENS":ENS})
        
        T = pd.DataFrame(new_rows)
        T.to_csv(output[0], sep="\t", index=False, header=False, columns = ["UCSC","ENS"])
        
rule download_UCSC_to_INSDC:
    output:
        "homo_sapiens/ucscToINSDC.txt.gz"
    shell:
        "curl http://hgdownload.soe.ucsc.edu/goldenPath/hg38/database/ucscToINSDC.txt.gz >{output[0]}"

rule download_genometest_galGal:
    output:
        "UCSC/homo_sapiens/hg19/liftover/hg19ToGalGal4.over.chain.gz"
    shell:
        "curl http://hgdownload-test.cse.ucsc.edu/goldenPath/hg19/liftOver/hg19ToGalGal4.over.chain.gz >{output[0]}"

rule make_rn5:
    input:
        "rattus_norvegicus/rn5.contigs",
        "rattus_norvegicus/Rnor_5.0.contigs"
    output:
        "UCSC/rattus_norvegicus/rn5/database/ucscToEnsembl.txt.gz"
    run:
        """
        turns out matches are so crappy, just ingore any chrUn"
        """
        ucsc_contigs = [l.rstrip() for l in open(input[0])]
        ens_contigs = [l.rstrip() for l in open(input[1])]
        #fewer ens so go through the ens 
        FOUT = open("UCSC/rattus_norvegicus/rn5/database/ucscToEnsembl.txt",'w')
        
        for ens_contig in ens_contigs:
            if not "AABR" in ens_contig and not "JH6" in ens_contig:
                ucsc_match, match = get_max_match(ens_contig, ucsc_contigs)
                print(ucsc_match, ens_contig)
                FOUT.write("%s\t%s\n"%(ucsc_match, ens_contig))
        FOUT.close()
        shell("gzip UCSC/rattus_norvegicus/rn5/database/ucscToEnsembl.txt")

rule make_rheMac:
    input:
        "macaca_mulatta/rheMac2.contigs",
        "macaca_mulatta/MMUL_1.contigs"
    output:
        "UCSC/macaca_mulatta/rheMac2/database/ucscToEnsembl.txt.gz"
    run:
        ucsc_contigs = [l.rstrip() for l in open(input[0])]
        ens_contigs = [l.rstrip() for l in open(input[1])]
        #fewer ucsc so go through the ucsc
        FOUT = open("UCSC/macaca_mulatta/rheMac2/database/ucscToEnsembl.txt",'w')
        for ucsc_contig in ucsc_contigs:
            if ucsc_contig != "chrUr":
                ens_contig, score = get_max_match(ucsc_contig, ens_contigs)
                print(ucsc_contig, ens_contig)
                FOUT.write("%s\t%s\n"%(ucsc_contig, ens_contig))
        FOUT.close()
        shell("gzip macaca_mulatta/ucscToEnsembl.txt")
    
